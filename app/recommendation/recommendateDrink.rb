class Recommendation
	attr_reader :selectors
	recommendator = {
		":name" => "name ILIKE '%value%'",
		":description" => "description ILIKE '%value%'",
		":alcohol_level" => "alcohol_level = value",
		":distilled" => "distilled is value",
		":temperature" => "temperature = value",
		":base_ingredient" => "base_ingredient ILIKE '%value%'",
		":origin" => "origin ILIKE '%value%'",
		":ibu" => "ibu = value",
		":drinkware" => "drinkware ILIKE '%value%'",
		":rating_avg" => "rating_avg = value"
	}

    #Definição da classe de seleção
	def initialize(keyWord)
		@selectors = keyWord
	end

    #Seleção de todos os resultados
	def new_recommendation
		recomFactors = []
		@selectors.each do |key, value|
			recomFactors << (recommendator[key]).('value', value)
        end
        #Buscar valor onde o seletor é localizado e ordenar por nome
		Drink.where(recomFactors.join(" and ")).order('name')
	end
end